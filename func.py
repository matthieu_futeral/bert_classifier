# -*- coding: utf-8 -*-

"""
Created on Mon Jul 22 00:42:39 2019

@author: matthieufuteral-peter
"""

import os
import sys
import re
import random
from pytorch_pretrained_bert import BertTokenizer, BertForMaskedLM
import torch
from tensorflow.python.keras.preprocessing import sequence
import numpy as np
from sklearn.manifold import TSNE
from sklearn.model_selection import train_test_split
from nltk.util import ngrams

PATH_PROJECT = os.getcwd()
PATH_DATA = os.path.join(PATH_PROJECT, 'data')

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# Preprocessing

#HappyEmoticons
emoticons_happy = [
    ':-)', ':)', ';)', ':o)', ':]', ':3', ':c)', ':>', '=]', '8)', '=)', ':}',
    ':^)', ':-D', ':D', '8-D', '8D', '=-D', '=D',
    '=-3', '=3', ':-))', ":'-)", ":')", ':*', ':^*', '>:P', ':-P', ':P',
    ':-p', ':p', '=p', ':-b', ':b', '>:)', '>;)', '>:-)',
    '<3'
    ]

#FunnyEmoticons
emoticons_fun = [
    'x-D', 'xD', 'X-D', 'XD', 'X-P',
    'x-p', 'xp', 'XP',
]

# Sad Emoticons
emoticons_sad = [
    ':L', ':-/', '>:/', ':S', '>:[', ':@', ':-(', ':[', ':-||', '=L', ':<',
    ':-[', ':-<', '=\\', '=/', '>:(', ':(', '>.<', ":'-(", ":'(", ':\\', ':-c',
    ':c', ':{', '>:\\', ';('
    ]


def emoji_preprocessing(text):
    count = 0
    for i in range(len(text)):
        s = text[i].split(' ')
        for j in range(len(s)):
            if s[j] in emoticons_happy:
                s[j] = 'happy'
                count += 1
            elif s[j] in emoticons_sad:
                s[j] = 'sad'
                count += 1
            elif s[j] in emoticons_fun:
                s[j] = 'lol'
                count += 1
            else:
                continue
        text[i] = ' '.join(s)

    return text, count


def Build_Sentences_and_Labels(paths, n_labels=3):
    '''
    Extract sentence and sentiment associated from data
    :return: couple of lists : text, label
    '''

    sentences = []
    labels = []

    doc = {0: 'negative', 1: 'neutral', 2: 'positive'}

    if type(paths) == str:
        paths = [paths]

    for path in paths:
        with open(path, 'r', encoding="utf8") as f:
            for v in f:
                v = v.split('\t')
                try:
                    text = v[2].split('\n')[0]
                except:
                    if len(v) != 4:
                        continue
                    else:
                        text = v[2]
                text = re.sub(r'pic.twitter.com\S+', '', text)
                text = re.sub(r'http\S+', '', text)
                text = re.sub(r'\\u2019', '\'', text)
                # text = re.sub(r'@\S+', 'username', text)
                # text = re.sub(r"[^0-9A-Za-zàâçéèêîïôîûùÀÇÉÈÙ ]", "", text)  # '’#()@/|\_:,.&~€%$-
                text = re.sub(r"  +", " ", text)
                text = text.lower().strip()
                # text = ' ' + text + ' '  # reconnaissance des mots en début et fin de phrase
                sentences.append(text)

                labels.append(list(doc.values()).index(v[1]))
                # try:
                #    note = int(v[1])
                #    if note != 2:  # phrases labelisées 2 ?
                #        sentences += [text]
                #        labels += [note + 1]
                # except:
                #    continue

    if n_labels == 2:
        doc = {0: 'negative', 1: 'positive'}

    sentences, _ = emoji_preprocessing(sentences)

    return sentences, labels, doc



def Build_sample(text, labels, random_state=42, n_labels=3, shuffle=True, bert_model='bert-base-cased'):
    '''
    convert into tokens and arrays for training step
    :param text: see above
    :param labels: see above
    :param num_labels: number of class
    :return: tokenized_padded text, train, test (txt and labels)
    '''
    tokenizer = BertTokenizer.from_pretrained(bert_model, do_lower_case=False)
    tokenized_text = []
    indexed_tokens = []

    if n_labels == 2:
        for i in range(len(text)):
            if labels[i] == 1:
                continue
            else:
                sentence = tokenizer.tokenize(text[i])
                sentence.append('[SEP]')
                sentence.insert(0, '[CLS]')
                tokenized_text.append(sentence)
                indexed_tokens.append(tokenizer.convert_tokens_to_ids(sentence))

        indexed_tokens = sequence.pad_sequences(indexed_tokens, padding='post', dtype='int64', maxlen=512)

        index = np.array(labels) != 1
        labels = np.array(labels)[index]
        labels[labels == 2] = 1
        labels = list(labels)

    else:
        for txt in text:
            sentence = tokenizer.tokenize(txt)
            sentence.append('[SEP]')
            sentence.insert(0, '[CLS]')
            tokenized_text.append(sentence)
            indexed_tokens.append(tokenizer.convert_tokens_to_ids(sentence))

        indexed_tokens = sequence.pad_sequences(indexed_tokens, padding='post', dtype='int64', maxlen=512)

    if shuffle:
        txt_train, txt_test, y_train, y_test = train_test_split(indexed_tokens, labels, test_size=0.2, random_state=random_state)
        txt_train = torch.Tensor(txt_train).long()
        txt_test = torch.Tensor(txt_test).long()
        y_train = torch.Tensor(y_train).long()
        y_test = torch.Tensor(y_test).long()  # Type long required for input and output

        return indexed_tokens, tokenized_text, txt_train, txt_test, y_train, y_test

    else:
        indexed_tokens = torch.Tensor(indexed_tokens).long()
        labels = torch.Tensor(labels).long()

        return tokenized_text, indexed_tokens, labels


def clean(L_):
    '''
    Inputs : tweets
    Return cleaned tweets - out of picture, links and emoji
    '''
    L = list(L_)
    for i in range(len(L)):
        L[i] = re.sub(r'pic.twitter.com\S+', '', L[i])
        L[i] = re.sub(r'http\S+', '', L[i])
        L[i] = re.sub(r'@\S+', '', L[i])
        L[i] = re.sub(r"[^0-9A-Za-zàâçéèêîïôùÀÇÉÈÙ' ]", "", L[i])  # '’#()@/|\_:,.&~€%$-
        L[i] = re.sub(r"  +", " ", L[i])
        L[i] = L[i].lower().strip()
    return L


def Tsne_from_LastTensor(X, tokens, id_tokens, n_components=3, perplexity=10, words=False):
    '''

    :param X: bag of sentences OR single sentence if words = True
    :param tokens: tokenized_text (multiple sentences or not)
    :param id_tokens: tokenized_ids
    :param n_components: 2d ou 3d for vizualisation
    :param words: if TRUE then single sentence in inputs, if FALSE bag of sentences
    :return: TSNE, or trained with sentences, or trained with words of a single sentence
    '''
    if words:
        assert sum(id_tokens.numpy() != 0) == len(tokens)
        y = X.numpy()[:len(tokens)]
        return TSNE(n_components=n_components, perplexity=perplexity).fit_transform(y)
    else:
        bag = X.numpy()  # 0 is [CLS] token's index, it is always the first element
        return TSNE(n_components=n_components, perplexity=perplexity).fit_transform(bag)


def extra_len(x, tokenizer):
    count = 0
    mean = 0
    for i in range(len(x)):
        if len(tokenizer.tokenize(x[i])) > 510:
            count += 1
            mean += len(tokenizer.tokenize(x[i])) - 510
    print('Count : {} - Mean extra len : {}'.format(count, mean/count))
    return count, mean/count


def preprocessing(x, tokenizer):
    s = tokenizer.tokenize(x)
    if len(s) > 510:
        s = s[:510]
    s.append('[SEP]')
    s.insert(0, '[CLS]')
    s = tokenizer.convert_tokens_to_ids(s)

    return s


def imdb_sampler(x, y, tokenizer):
    x = list(x.apply(lambda x: preprocessing(x, tokenizer)))
    x = sequence.pad_sequences(x, padding='post', dtype='int64', maxlen=512)

    label = list(set(y))
    doc = {i: label[i] for i in range(len(label))}
    inverse_doc = {doc[i]: i for i in doc.keys()}

    y_preprocessed = []
    for i in range(len(y)):
        y_preprocessed += [inverse_doc[y[i]]]

    return torch.Tensor(x).long(), torch.Tensor(y_preprocessed).long(), doc


def preprocessing_MLM(text, p=0.15):
    tokenizer = BertTokenizer.from_pretrained('bert-base-cased', do_lower_case=False)
    tokenized_text = []
    indexed_tokens = []
    labels = []

    for i in range(len(text)):
        label = []
        sentence = tokenizer.tokenize(text[i])
        if len(sentence) > 510:
            sentence = sentence[:510]

        for j in range(len(sentence)):
            label.append(sentence[j])
            if random.random() < p:
                sentence[j] = '[MASK]'
            else:
                continue
        sentence.append('[SEP]')
        label.append('[SEP]')
        sentence.insert(0, '[CLS]')
        label.insert(0, '[CLS]')

        tokenized_text.append(sentence)
        indexed_tokens.append(tokenizer.convert_tokens_to_ids(sentence))
        labels.append(tokenizer.convert_tokens_to_ids(label))

    indexed_tokens = sequence.pad_sequences(indexed_tokens, padding='post', dtype='int64', maxlen=512)
    labels = sequence.pad_sequences(labels, padding='post', dtype='int64', maxlen=512)
    indexed_tokens = torch.Tensor(indexed_tokens).long()
    labels = torch.Tensor(labels).long()

    return tokenized_text, indexed_tokens, labels

def MLM_preprocess_2(text):
    tokenizer = BertTokenizer.from_pretrained('bert-base-cased', do_lower_case=False)
    tokenized_text = []
    indexed_tokens = []

    for i in range(len(text)):
        sentence = tokenizer.tokenize(text[i])
        if len(sentence) > 510:
            sentence = sentence[:510]

        sentence.append('[SEP]')
        sentence.insert(0, '[CLS]')

        tokenized_text.append(sentence)
        indexed_tokens.append(torch.Tensor(tokenizer.convert_tokens_to_ids(sentence)).long())

    indexed_tokens = sequence.pad_sequences(indexed_tokens, padding='post', dtype='int64')

    return tokenized_text, indexed_tokens


def ngram_count(text, n):

    count = 0
    n_grams = []
    result = {}

    if type(text) == str:
        text = [text]

    for sentence in text:
        l = sentence.split()

        for w in l:
            ng = list(ngrams(w, n))
            n_grams += ng
            count += len(n_grams)

            keys = set(ng)
            for k in keys:
                if k in result.keys():
                    result[k] += ng.count(k)
                else:
                    result[k] = ng.count(k)

    return count, n_grams, result

