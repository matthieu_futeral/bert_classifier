# -*- coding: utf-8 -*-

"""
Created on Mon Jul 22 00:42:39 2019

@author: matthieufuteral-peter
"""

import os
import sys
import torch
import argparse
from pytorch_pretrained_bert import BertForSequenceClassification
from func import Build_sample, Build_Sentences_and_Labels
from Bert_SentimentAnalysis import RunModel_SentimentAnalysis


parser = argparse.ArgumentParser(description='grid_search_classifier')
parser.add_argument('--epochs', type=int, default=30)
parser.add_argument('--bert_model', type=str, default='bert-base-cased')
parser.add_argument('--patience', type=int, default=3)
args = parser.parse_args()

PATH_PROJECT = os.getcwd()
PATH_MODEL = os.path.join(PATH_PROJECT, 'models', 'english_set')
PATH_REPORT = os.path.join(PATH_PROJECT, 'report', 'english_set')
PATH_DATA = os.path.join(PATH_PROJECT, 'data')
if not os.path.exists(PATH_MODEL + '/grid_search'):
    dirName = PATH_MODEL + '/grid_search'
    os.mkdir(dirName)
    print("Directory ", dirName,  " Created ")

paths = [PATH_DATA + '/twitter-' + str(n) + 'train-A.txt' for n in [2013, 2015, 2016]]
paths += [PATH_DATA + '/twitter-' + str(n) + 'test-A.txt' for n in range(2013, 2017)]
paths += [PATH_DATA + '/twitter-' + str(n) + 'dev-A.txt' for n in [2013, 2016]]

test_path = PATH_DATA + '/test_semeval2017-A.txt'

learning_rate = [1e-4, 1e-5, 1e-6, 1e-7]
batch_size = [1, 2, 4, 8]
dropout = [0.1, 0.2, 0.5, 0.75]

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

train_text, train_labels, _ = Build_Sentences_and_Labels(paths)
test_text, test_labels, doc = Build_Sentences_and_Labels(test_path)
print(doc)

train_indexed_tokens, X_train, y_train = Build_sample(train_text, train_labels, random_state=None, shuffle=False, bert_model=args.bert_model)
test_indexed_tokens, X_test, y_test = Build_sample(test_text, test_labels, random_state=None, shuffle=False, bert_model=args.bert_model)

for bs in batch_size:
    for lr in learning_rate:
        for dr in dropout:
            print('{} - {:.0e} - {}'.format(bs, lr, dr))
            model = BertForSequenceClassification.from_pretrained(args.bert_model, num_labels=3)
            model, epoch_loss, batch_loss, accuracy, tpe = RunModel_SentimentAnalysis(X_train, y_train, epochs=args.epochs,
                                                                                      batch_size=bs, model=model,
                                                                                      lr=lr, report=1, evaluate=1,
                                                                                      path_model=PATH_MODEL + '/grid_search',
                                                                                      path_report=PATH_REPORT, patience=args.patience)



