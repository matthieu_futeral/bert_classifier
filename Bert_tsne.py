# -*- coding: utf-8 -*-

"""
Created on Mon Jul 22 00:42:39 2019

@author: matthieufuteral-peter
"""

import sys
import os
import argparse
import pdb
import numpy as np
import torch
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
from pytorch_pretrained_bert import BertForSequenceClassification, BertTokenizer, BertModel
from func import Tsne_from_LastTensor, Build_sample, Build_Sentences_and_Labels
np.random.seed(21)

parser = argparse.ArgumentParser(description='TNSE')
parser.add_argument('--model_name', type=str, default='dc5da')
parser.add_argument('--trained', type=int, default=1)
parser.add_argument('--n_sentences', type=int, default=50)
parser.add_argument('--perplexity', type=int, default=10)
parser.add_argument('--debug', type=int, default=0)
parser.add_argument('--neutral', type=int, default=1)
parser.add_argument('--display_pred', type=int, default=1)
args = parser.parse_args()

if not args.debug:
    pdb.set_trace = lambda: None

# Connect to plotly
plotly.tools.set_credentials_file(username='mfuteralpeter', api_key='M0ERZXxVSY120jL9wm3b')

'''
Choose randomly N sentences from SemEval2017 dataset and reduce the dimension. (PCA & TSNE)
Last layer here [CLS], it could be interesting to compare with other layers
'''

PATH_PROJECT = os.getcwd()
PATH_MODEL = os.path.join(PATH_PROJECT, 'models', 'english_set', args.model_name)
PATH_DATA = os.path.join(PATH_PROJECT, 'data')

test_path = PATH_DATA + '/test_semeval2017-A.txt'

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

if args.trained:
    model = BertForSequenceClassification.from_pretrained('bert-base-cased', num_labels=3)
    model.load_state_dict(torch.load(PATH_MODEL + '/checkpoint.pt', map_location=device.__str__()))
    model = model.bert
    name = 'tsne_after_training_perplexity{}_nsentences{}_pred{}'.format(args.perplexity, args.n_sentences, args.display_pred)
else:
    model_bert = BertModel.from_pretrained('bert-base-cased')
    name = 'tnse_before_training'
model.eval()

# Prepare dataset
text, labels, doc = Build_Sentences_and_Labels(test_path)
if not args.neutral:
    text = [txt for i, txt in enumerate(text) if labels[i] != 1]
    labels = [lab for lab in labels if lab != 1]
print(doc)
tokenized_text, indexed_tokens, labels = Build_sample(text, labels, shuffle=False)
max_sentence_length = indexed_tokens.shape[1]

# Load pre-trained model tokenizer
tokenizer = BertTokenizer.from_pretrained('bert-base-cased', do_lower_case=False)


def extract_embeddings(N, liste=[], random=True, model=model, perplexity=10):
    '''
    Extract [CLS] embeddings only

    Could take more than 2-3 minutes if N is large

    :param N: number of sentences to extract, N > 1, else use torch.unsqueeze(sentence, dim = 0)
    :param liste: if random = False, needs to be filled in with a list of indexes
    :param random: Choose randomly sentences
    :return: prepared tsne + indexes
    '''
    if random:
        indexes = np.random.randint(0, len(tokenized_text)-1, N)
        pdb.set_trace()
        sample = indexed_tokens[indexes]
        print(indexes)
    else:
        pdb.set_trace()
        sample = indexed_tokens[liste]
        indexes = liste
        print(indexes)

    with torch.no_grad():
        _, bag_sentences = model(sample, attention_mask=(sample != 0), output_all_encoded_layers=False)

    tsne = Tsne_from_LastTensor(bag_sentences, np.array(tokenized_text)[indexes], indexed_tokens[indexes],
                                n_components=3, perplexity=perplexity)

    if args.trained and args.display_pred:
        model = BertForSequenceClassification.from_pretrained('bert-base-cased', num_labels=3)
        model.load_state_dict(torch.load(PATH_MODEL + '/checkpoint.pt', map_location=device.__str__()))
        output = model.forward(sample, attention_mask=(sample != 0))
        pred = torch.argmax(output, 1)

    x = tsne[:, 0]
    y = tsne[:, 1]
    z = tsne[:, 2]

    if args.display_pred:
        if random:
            return indexes, x, y, z, pred
        else:
            return liste, x, y, z, pred
    else:
        if random:
            return indexes, x, y, z
        else:
            return liste, x, y, z


'''
Data Vizualisation : Tsne 3_dimensions with plotly
'''

color = {'negative': 'red', 'neutral': 'grey', 'positive': 'blue'}


def data_viz(index, x, y, z, name='tsne_cls', display_pred=1):

    if not args.display_pred:
        sentiment = np.array(labels)[index]
    else:
        sentiment = pred.numpy()

    liste_color = [color[doc[s]] for s in sentiment]

    trace = go.Scatter3d(
        x=x,
        y=y,
        z=z,
        text=list(np.array(text)[index]),
        mode='markers', #+text',
        marker=dict(
            color=liste_color,
            size=12,
            symbol='circle',
            line=dict(
                color='rgb(204, 204, 204)',
                width=1
            ),
            opacity=1.0
        )
    )

    data = [trace]
    layout = go.Layout(
        margin=dict(
            l=0,
            r=0,
            b=0,
            t=0
        )
    )

    fig = go.Figure(data=data, layout=layout)
    py.iplot(fig, filename=name)

    return fig.show()


if __name__ == '__main__':
    if args.display_pred:
        index, x, y, z, pred = extract_embeddings(args.n_sentences, random=True, model=model, perplexity=args.perplexity)
    else:
        index, x, y, z = extract_embeddings(args.n_sentences, random=True, model=model, perplexity=args.perplexity)

    data_viz(index, x, y, z, name=name, display_pred=args.display_pred)




