# -*- coding: utf-8 -*-

"""
Created on Mon Aug 22 00:42:39 2019

@author: matthieufuteral-peter
"""

import os
import sys
import time
import argparse
import torch
import pdb
from pytorch_pretrained_bert import BertForSequenceClassification
from func import Build_sample
from uuid import uuid4

RUN_ID = str(uuid4())[0:5]
parser = argparse.ArgumentParser(description='Text')
parser.add_argument('--txt', type=str, help='sentence_to_predict')
parser.add_argument('--label', type=int, help='0-Neg-1-Neu-2-Pos')
parser.add_argument('--model', type=str, default='dc5da')
parser.add_argument('--bert_model', type=str, default='bert-base-cased')
parser.add_argument('--num_labels', type=int, default=3)
parser.add_argument('--model_set', type=str, default='imdb', help='imdb or english_set')
args = parser.parse_args()

PATH_PROJECT = os.getcwd()
PATH_TEXT = os.path.join(PATH_PROJECT, 'test_results')
if args.model_set == 'imdb':
    PATH_MODEL = os.path.join(PATH_PROJECT, 'models', 'imdb', args.model)
elif args.model_set == 'english_set':
    PATH_MODEL = os.path.join(PATH_PROJECT, 'models', 'english_set', args.model)
else:
    raise Exception('Dataset not found')

f = open(os.path.join(PATH_TEXT, RUN_ID + '.txt'), 'w')

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = BertForSequenceClassification.from_pretrained(args.bert_model, num_labels=args.num_labels)
model.load_state_dict(torch.load(PATH_MODEL + '/checkpoint.pt', map_location=device.__str__()))
model.to(device)
model.eval()

_, sentence, label = Build_sample([args.txt], [args.label], shuffle=False, bert_model=args.bert_model)

output = model(sentence)
prediction = torch.argmax(output).item()

f.write(args.txt + '\n' + 'label = ' + str(args.label) + '\n' + 'prediction = ' + str(prediction))
f.close()





