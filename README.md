## Introduction

This project aims to complete the [SemEval2017 Task 4 Subtask A](http://alt.qcri.org/semeval2017/task4/) in English which is a Sentiment Analysis with 3 classes task (Negative, Neutral and Positive sentiment). Train and test data are available [here](http://alt.qcri.org/semeval2017/task4/index.php?id=download-the-full-training-data-for-semeval-2017-task-4) and [here](http://alt.qcri.org/semeval2017/task4/index.php?id=data-and-tools) respectively.

To achieve it, a BERT pre-trained model is fine-tuned and then used with a classifier layer randomly initialized on top. Pytorch BERT pre-trained model could and should be downloaded on [huggingface's git](https://github.com/huggingface/pytorch-transformers#doc).

To use this project, please create **data**, **models**, **report** and **test_results** folders.

## Dependencies

- PyTorch
- Numpy
- pytorch-pretrained-bert
- Tensorflow (preprocessing)
- Scikit-learn (Vizualization)



## Training Step 

Model is trained with 48248 samples (sarcasm dataset is excluded) and evaluated on 12284 samples according to the data provided by the SemEval2017.\
For more information about the competition or the data used, please have a look to [competition's details](http://alt.qcri.org/semeval2017/task4/data/uploads/semeval2017-task4.pdf) and check Subtask A paragraph.

The idea is to compare SemEval2017's best results with a fine-tuned BERT pre-trained model. Early stopping is implemented to stop the training step when overfitting : check [here](https://github.com/Bjarten/early-stopping-pytorch) for more details about the implementation.

- To train a model :
```
python Bert_SentimentAnalysis.py --epochs 30 --batch_size 1 --lr 1e-6 --save 1 --eval 1 --early_stopping 1 --dropout 0.5 --bert_model bert-base-multilingual-cased --weight 1 --dataset english_set --n_labels 3
```

### Parameters

- gpu : 0 or 1 - using gpu or not
- report : 0 or 1 - report training step on tensorboard, please see [lanpa's git](https://github.com/lanpa/tensorboardX) for more details
- early-stopping : 0 or 1 - stop the training when the validation loss does not decrease anymore
- save : 0 or 1 - save model's parameters after training and scores
- patience : int (1 to n_epochs) - how many epochs to wait before stopping the training step (see early-stopping above)
- dropout : float - from 0 to 1 - select randomly a part of the input not to take into account during training step
- bert_model : 'bert-base-multilingual-cased' or 'bert-base-cased' mainly for English - see [huggingface](https://github.com/huggingface/pytorch-transformers#doc) for more details
- weight : 0 or 1 - Weighted backpropogation or not (to tackle imbalanced class)
- dataset : 'imdb' or 'english_set' - Either to train on imdb dataset or Semeval dataset. Please to train a model on imdb's data look at imdb_bert.py
- n_labels : int - 2 or 3 - Train on all classes or only positive/negative.


## Files

- **Bert_SentimentAnalysis.py** is main. It allows to train the model. See above for parameters.
- **func.py** runs all the main functions used for this project.
- **grid_search.py** finds best parameters to outperform previous results on this competition.
```
python grid_search.py --epochs 30 --bert_model bert-base-cased --patience 3
```
- **prediction.py** : --txt your_text --label your_label --model model RUN_ID, returns a .txt file with sentence and prediction.
**Please mind that model's RUN_ID matches with bert_model, num_labels and model_set. Otherwise will return an error.**
```
python prediction.py --txt your_text --label your_label --model model's RUN_ID --bert_model bert-base-cased --num_labels 3 --model_set english_set
```


- **Bert_tsne** : --model_name model's RUN_ID --trained 1 or 0 (using a trained model or not, if 1 please enter a correct model_name) --n_sentences 50. Plot a Tsne of random sentences (using [CLS] layer).
```
python Bert_tsne.py --model_name dc5da --trained 1 --n_sentences 50
```

- **imdb_bert** trains bert on imdb dataset (2 classes negative - positive).
```
python imdb_bert.py --epochs 30 --batch_size 1 --lr 1e-5 --bert_model bert-base-cased --dropout 0.2
```
- **pytorchtools.py** implements early stopping in pytorch. Please see [Bjarten's git](https://github.com/Bjarten/early-stopping-pytorch/blob/master/pytorchtools.py) for more information.
- **bertviz** shows attentions on every layer : please see [here](https://github.com/jessevig/bertviz) and have a look at [Jesse Vig. 2019. A Multiscale Visualization of Attention in the Transformer Model. arXiv:1906.05714v1](https://arxiv.org/pdf/1906.05714.pdf).


## Results

Fine-tuned Bert pre-trained model improved state-of-the-art on the SemEval2017 Task 4 Subtask A dataset in every metric.\
Best model reached :
>
>> **AvgRecall** : 0.696 - **F1_PN** : 0.692 - **Accuracy** : 0.699
>

The preprocessing steps were the same as those implemented by the winner of the competition - [BB twtr at SemEval-2017 Task 4: Twitter Sentiment Analysis with CNNs and LSTMs](https://aclweb.org/anthology/S17-2094).
The results and the scores of the competition are available [here](http://alt.qcri.org/semeval2017/task4/index.php?id=results).

![][1]

[1]:
https://nsa40.casimages.com/img/2019/09/12/190912021426721946.png
"Results on Semeval's competition - BB_twtr and DataStories are winners of the Semeval 2017. Base and multilingual are BERT's version. Weighted means loss had been weighted by classes distribution"
*Comparison between best Bert models and scores of the Competition's winners (DataStories and BB_twtr). Weighted means the model was trained with a weighted loss by classes distribution. Base and Multilingual refer to bert's version.*

## Going further

To eval bert's performance on out-of-domain noisy data, the idea is to fine-tuned a Bert pre-trained model on IMDB dataset ([available here](https://www.kaggle.com/iarunava/imdb-movie-reviews-dataset)) and then evaluate it on Semeval test set keeping only positive and negative classes.
About 3800 examples in the Imdb train and test set exceed the 512 tokens threshold of Bert. So, for theses examples, the review is cut at 512 tokens and the end is not taken into account.

To compare, it is necessary to train a new model on Semeval dataset keeping only positive and negative classes. Please mind to specify n_labels when training.

Results are as follow :

- **Model trained on Semeval dataset (in-domain noisy data) and evaluate on Semeval test set**

>
>> **AvgRecall** : 0.910 - **F1_PN** : 0.903 - **Accuracy** : 0.908
>

- **Model trained on Imdb dataset (out-of-domain data) and evaluate on Semeval test set**

>
>> **AvgRecall** : 0.790 - **F1_PN** : 0.798 - **Accuracy** : 0.816
>

![][2]

[2]:
https://chart-studio.plot.ly/~mfuteralpeter/24.png
"Performances, on Semeval test set, of BERT models trained on in-domain and out-of-domain datasets"
*Performances of both models on every metric (2 classes : positive and negative).*