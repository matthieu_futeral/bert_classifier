# -*- coding: utf-8 -*-

"""
Created on Mon Jul 22 00:42:39 2019

@author: matthieufuteral-peter
"""

import os
import sys
import json
import time
import argparse
import torch
import pdb
from tensorboardX import SummaryWriter
from pytorchtools import EarlyStopping
import torch.nn as nn
import torch.optim as optim
import numpy as np
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data import TensorDataset
from pytorch_pretrained_bert import BertForSequenceClassification
from func import Build_sample, Build_Sentences_and_Labels
from uuid import uuid4


RUN_ID = str(uuid4())[0:5]
print('RUN_ID : {}'.format(RUN_ID))
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

parser = argparse.ArgumentParser(description='Bert_sentiment_classifier')
parser.add_argument('--epochs', type=int, help='n_epochs', default=1)
parser.add_argument('--batch_size', type=int, help='batch_size', default=1)
parser.add_argument('--lr', type=float, help='learning_rate', default=1e-5)
parser.add_argument('--gpu', type=int, help='gpu', default=1)
parser.add_argument('--report', type=int, help='tensorboardX_reporting', default=1)
parser.add_argument('--debug', type=int, help='debug', default=0)
parser.add_argument('--early_stopping', type=int, help='early_stopping', default=1)
parser.add_argument('--random_state', type=int, default=42)
parser.add_argument('--evaluate', type=int, default=1)
parser.add_argument('--save', type=int, default=1)
parser.add_argument('--patience', type=int, default=5)
parser.add_argument('--dropout', type=float, default=0)
parser.add_argument('--bert_model', type=str, default='bert-base-multilingual-cased')
parser.add_argument('--weight', type=int, default=0)
parser.add_argument('--dataset', type=str, default='imdb', help='imdb or english_set')
parser.add_argument('--n_labels', type=int, default=3)
args = parser.parse_args()

PATH_PROJECT = os.getcwd()

if not args.debug:
    pdb.set_trace = lambda: None


# Working space

# Training step

def RunModel_SentimentAnalysis(X_train, y_train, X_test, y_test, epochs=5, batch_size=1, lr=1e-6, valid_size=0.25, model=None,
                               affichage=False, report=1, evaluate=1, gpu=1, early_stopping=1, patience=5, path_model=None,
                               path_report=None, weight=0, num_labels=3, doc=None):
    '''
    Train BertForSequenceClassification on twitter
    '''

    if torch.cuda.is_available() and gpu:
        model.to(device)


    if report and torch.cuda.is_available():
        report_name = path_report + '/runs/' + RUN_ID + '_{:.0e}_bs{}_{}/'.format(lr, batch_size,
                                                                                  os.environ.get('OAR_JOB_ID'))
        os.mkdir(report_name)
        writer = SummaryWriter(logdir=report_name, comment='SentimentClassification-' + RUN_ID + '-' + os.environ.get('OAR_JOB_ID'))
    elif report and not torch.cuda.is_available():
        report_name = path_report + '/runs/' + RUN_ID + '_{:.0e}_bs{}/'.format(lr, batch_size)
        os.mkdir(report_name)
        writer = SummaryWriter(logdir=report_name, comment='SentimentClassification-' + RUN_ID)


    if args.save and torch.cuda.is_available():
        PATH = os.path.join(path_model, RUN_ID + '_' + os.environ.get('OAR_JOB_ID'))
        os.mkdir(PATH)
    elif args.save and not torch.cuda.is_available():
        PATH = os.path.join(path_model, RUN_ID)
        os.mkdir(PATH)


    if early_stopping and not torch.cuda.is_available():
        early_stopping = EarlyStopping(patience=patience, RUN_ID=RUN_ID, verbose=True,
                                       path_model=os.path.join(path_model, RUN_ID))
    elif early_stopping and torch.cuda.is_available():
        early_stopping = EarlyStopping(patience=patience, RUN_ID=RUN_ID, verbose=True,
                                       path_model=os.path.join(path_model, RUN_ID + '_' + os.environ.get('OAR_JOB_ID')))
    # pdb.set_trace()

    # create datasets
    train_set = TensorDataset(X_train, y_train)
    num_train = len(X_train)
    indices = list(range(num_train))
    np.random.shuffle(indices)
    split = int(np.floor(valid_size * num_train))
    train_idx, valid_idx = indices[split:], indices[:split]

    train_sampler = SubsetRandomSampler(train_idx)
    valid_sampler = SubsetRandomSampler(valid_idx)

    # load training data in batches
    train_loader = torch.utils.data.DataLoader(train_set,
                                               batch_size=batch_size,
                                               sampler=train_sampler,
                                               num_workers=0)

    # load validation data in batches
    valid_loader = torch.utils.data.DataLoader(train_set,
                                               batch_size=4,
                                               sampler=valid_sampler,
                                               num_workers=0)

    # Prepare training step
    optimizer = optim.Adam(model.parameters(), lr=lr)  # ADAM, SGD

    if weight and num_labels == 3:
        criterion = nn.CrossEntropyLoss(torch.Tensor([1.0, 0.3413709640590035, 0.39600550964187325]).to(device))
    elif weight and num_labels == 2:
        criterion = nn.CrossEntropyLoss(torch.Tensor([1.0, 0.39600550964187325]).to(device))
    else:
        criterion = nn.CrossEntropyLoss()

    epoch_loss, val_loss, tpe = [], [], 0
    accuracy = {'train': [], 'val': [], 'test': []}

    for epoch in range(epochs):

        model.train()
        batchs = len(X_train)//batch_size
        batch_loss = []
        acc = []
        te = time.time()

        for batch, (X_sample, target) in enumerate(train_loader, 1):

            if affichage:
                print(X_sample)

            if torch.cuda.is_available() and gpu:
                X_sample = X_sample.to(device)
                target = target.to(device)

            # pdb.set_trace()  # check if cuda is available

            optimizer.zero_grad()
            output = model.forward(X_sample, attention_mask=(X_sample != 0))
            # training mode => Dropout is activated so outputs are not the same every time we compute it
            loss = criterion(output, target)
            loss.backward()
            optimizer.step()

            batch_loss.append(loss.item())
            acc.append(torch.sum(torch.argmax(output, 1) == target).item() / batch_size)
            # pdb.set_trace()

            if report:
                n_iter = batch + epoch*batchs

                if batch % 1000 == 0:
                    writer.add_histogram('word_embedding', model.bert.embeddings.word_embeddings.weight, n_iter)
                    writer.add_histogram('grad_word_embedding', model.bert.embeddings.word_embeddings.weight.grad, n_iter)
                    for i in range(12):
                        writer.add_histogram('layer_query'+str(i), model.bert.encoder.layer[i].attention.self.query.weight, n_iter)
                        writer.add_histogram('grad_layer_query' + str(i),
                                              model.bert.encoder.layer[i].attention.self.query.weight.grad, n_iter)
                        writer.add_histogram('layer_key' + str(i),
                                              model.bert.encoder.layer[i].attention.self.key.weight, n_iter)
                        writer.add_histogram('grad_layer_key' + str(i),
                                             model.bert.encoder.layer[i].attention.self.key.weight.grad, n_iter)
                        writer.add_histogram('layer_value' + str(i),
                                              model.bert.encoder.layer[i].attention.self.value.weight, n_iter)
                        writer.add_histogram('grad_layer_value' + str(i),
                                              model.bert.encoder.layer[i].attention.self.value.weight.grad, n_iter)
                    writer.add_histogram('classifier', model.classifier.weight, n_iter)
                    writer.add_histogram('grad_classifier', model.classifier.weight.grad, n_iter)
                    writer.add_histogram('bias_classifier', model.classifier.bias, n_iter)

        current_loss = np.mean(batch_loss)
        current_accuracy = np.mean(acc)
        accuracy['train'].append(current_accuracy)
        epoch_loss.append(current_loss)
        tpe += time.time() - te

        print("Train Sample - Epoch {} - Loss : {:.5f} - Accuracy : {:.3f}".format(epoch + 1, current_loss, current_accuracy))

        if report:
            writer.add_scalar('data/epoch_loss', current_loss, epoch + 1)
            writer.add_scalar('data/accuracy', current_accuracy, epoch + 1)
            print('Report done')

        if evaluate:
            model.eval()
            val_acc = []
            val_batch_loss = []
           # pdb.set_trace()

            for data, target in valid_loader:

                if torch.cuda.is_available() and gpu:
                    data = data.to(device)
                    target = target.to(device)

                output = model.forward(data, attention_mask=(data != 0))
                loss = criterion(output, target)
                val_batch_loss.append(loss.item())
                val_acc.append(torch.sum(torch.argmax(output, 1) == target).item() / 4)
              # pdb.set_trace()

            valid_loss = np.mean(val_batch_loss)
            val_loss.append(valid_loss)
            accuracy['val'].append(np.mean(val_acc))

            writer.add_scalar('data/val_accuracy', np.mean(val_acc), epoch + 1)
            writer.add_scalar('data/loss_val', np.mean(val_batch_loss), epoch + 1)

            print('Validation Sample - Epoch {} - Loss : {:.5f} - Accuracy : {:.3f}'.format(epoch + 1, valid_loss, np.mean(val_acc)))

        '''
        Early Stopping
        '''
        early_stopping(valid_loss, model)
        if early_stopping.early_stop:
            print("Early stopping")
            break

    if torch.cuda.is_available():
        PATH = os.path.join(path_model, RUN_ID + '_' + os.environ.get('OAR_JOB_ID'))
        model.load_state_dict(torch.load(PATH + '/checkpoint.pt'))
    else:
        PATH = os.path.join(PATH_MODEL, RUN_ID)
        model.load_state_dict(torch.load(PATH + '/checkpoint.pt'))

    # Load test data in batchs
    if evaluate:

        test_set = TensorDataset(X_test, y_test)
        test_loader = torch.utils.data.DataLoader(test_set,
                                              batch_size=4,
                                              num_workers=0)

        model.eval()
        test_acc = []
        test_batch_loss = []
        correct_output = [0]*num_labels
        prediction = [0]*num_labels

        for data, target in test_loader:

            if torch.cuda.is_available() and args.gpu:
                data = data.to(device)
                target = target.to(device)

            output = model.forward(data, attention_mask=(data != 0))
            loss = nn.CrossEntropyLoss()(output, target)
            test_batch_loss.append(loss.item())
            test_acc.append(torch.sum(torch.argmax(output, 1) == target).item() / 4)

            for i in range(len(target)):
                prediction[torch.argmax(output, 1)[i].item()] += 1
                if torch.argmax(output, 1)[i] == target[i]:
                    correct_output[target[i]] += 1

        recall = [correct_output[i]/torch.sum(y_test == i).item() for i in doc.keys()]
        precision = [correct_output[i]/prediction[i] for i in doc.keys()]
        F1_score = [2*recall[i]*precision[i]/(recall[i] + precision[i]) for i in doc.keys()]
        test_loss = np.mean(test_batch_loss)
        accuracy['test'].append(np.mean(test_acc))
        AvgRecall = np.mean(recall)
        F1_PN = (F1_score[0] + F1_score[-1])/2

        print("Test sample - Loss : {:.6} - Accuracy : {:.4}".format(test_loss, np.mean(test_acc)))

    if report:
        writer.export_scalars_to_json(path_report + '/all_scalars' + RUN_ID + '_{:.0e}_bs{}'.format(lr, batch_size) + '.json')
        writer.close()

    if args.save:
        path = PATH
        ind = np.argmax(accuracy['val'])

        data = {"metric": "accuracy", "info_score": None, "score": (accuracy['train'][ind], accuracy['val'][ind], accuracy['test'][-1]),
                "recall": recall, "precision": precision, "F1_score": F1_score, "AvgRecall": AvgRecall, "F1_PN": F1_PN,
                "model_full_name": "Bert_sentiment_classifier" + RUN_ID,
                "n_tokens_score": None, "n_sents": None, "token_type": "sub-word", "subsample": "all",
                "avg_per_sent": 0,
                "min/epoch": None,
                "model_args_dir": path + '/' + RUN_ID + "_args.json",
                "evaluation_script": "exact_match", "task": ["classification"], "data": "lexnorm"}

        param = {"checkpoint_dir": path + '/checkpoint.pt',
                 "hyperparameters": {"bert_model": args.bert_model, "lr": args.lr,
                                     "n_epochs": args.epochs,
                                     "initialize_bpe_layer": None,
                                     "weighted_loss": args.weight,
                                     "fine_tuning_strategy": "standard", "dropout_input_bpe": 0.0, "heuristic_ls": None,
                                     "gold_error_detection": 0, "dropout_classifier": args.dropout, "dropout_bert": args.dropout,
                                     "tasks": ["classification"], "masking_strategy": None, "portion_mask": None,
                                     "checkpoint_dir": None,
                                     "norm_2_noise_training": None, "random_iterator_train": True,
                                     "aggregating_bert_layer_mode": "last",
                                     "tokenize_and_bpe": 0, "SEED": None, "case": "lower",
                                     "bert_module": "Bert_ForSequenceClassification", "freeze_layer_prefix_ls": None,
                                     "layer_wise_attention": 0, "append_n_mask": 0},
                 "info_checkpoint": {"n_epochs": args.epochs, "batch_size": args.batch_size,
                                     "train_path": "lex_norm2015_split_train_2",
                                     "dev_path": "lex_norm2015_split_dev_2"}}

        with open(PATH + '/' + RUN_ID + "_args.json", 'w') as f:
            json.dump(data, f)
        with open(PATH + '/' + RUN_ID + "_param.json", 'w') as f:
            json.dump(param, f)

    tpe = tpe/epochs

    print("Average time per epoch : {:.5f}".format(tpe))

    return model, epoch_loss, val_loss, accuracy, tpe


# check whether the model is training or not with boolean model.training

### Training step

if __name__ == "__main__":

    print('Dataset is : {}'.format(args.dataset))

    PATH_MODEL = os.path.join(PATH_PROJECT, 'models', args.dataset)
    if not os.path.exists(PATH_MODEL):
        os.mkdir(PATH_MODEL)

    PATH_REPORT = os.path.join(PATH_PROJECT, 'report', args.dataset)
    if not os.path.exists(PATH_REPORT):
        os.mkdir(PATH_REPORT)

    PATH_DATA = os.path.join(PATH_PROJECT, 'data')
    paths = [PATH_DATA + '/twitter-' + str(n) + 'train-A.txt' for n in [2013, 2015, 2016]]
    paths += [PATH_DATA + '/twitter-' + str(n) + 'test-A.txt' for n in range(2013, 2017)]
    paths += [PATH_DATA + '/twitter-' + str(n) + 'dev-A.txt' for n in [2013, 2016]]
    test_path = PATH_DATA + '/test_semeval2017-A.txt'

    train_text, train_labels, _ = Build_Sentences_and_Labels(paths, n_labels=args.n_labels)
    test_text, test_labels, doc = Build_Sentences_and_Labels(test_path, n_labels=args.n_labels)
    print(doc)

    train_indexed_tokens, X_train, y_train = Build_sample(train_text, train_labels, random_state=None, shuffle=False,
                                                          n_labels=args.n_labels, bert_model=args.bert_model)
    test_indexed_tokens, X_test, y_test = Build_sample(test_text, test_labels, random_state=None, shuffle=False,
                                                       n_labels=args.n_labels, bert_model=args.bert_model)

    max_sentence_length = max(X_train.shape[1], X_test.shape[1])

    model = BertForSequenceClassification.from_pretrained(args.bert_model, num_labels=args.n_labels)
    # output_attentions = True
    if args.dropout != 0:
        model.dropout.p = args.dropout

    model, epoch_loss, val_loss, accuracy, tpe = RunModel_SentimentAnalysis(X_train, y_train, X_test, y_test,
                                                                            epochs=args.epochs, batch_size=args.batch_size,
                                                                            lr=args.lr, affichage=False, report=args.report, model=model,
                                                                            evaluate=args.evaluate, gpu=args.gpu, early_stopping=args.early_stopping,
                                                                            patience=args.patience, weight=args.weight, path_model=PATH_MODEL,
                                                                            path_report=PATH_REPORT, num_labels=args.n_labels, doc=doc)


# Download weights
    # model = BertForSequenceClassification.from_pretrained('bert-base-multilingual-cased', num_labels=3)
    # model.load_state_dict(torch.load(path + 'model.tar'))
    # model.eval()

# tensorboard --logdir=/Users/matthieufuteral-peter/Desktop/Inria/sosweet_1/bert_classifier/report/ --host localhost --port 8088


