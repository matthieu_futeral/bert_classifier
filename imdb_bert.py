# -*- coding: utf-8 -*-

"""
Created on Mon Jul 22 00:42:39 2019

@author: matthieufuteral-peter
"""

import os
import sys
import argparse
import pandas as pd
import numpy as np
from pytorch_pretrained_bert import BertForSequenceClassification, BertTokenizer
import torch
import torch.nn as nn
from torch.utils.data import TensorDataset
import pdb
from func import extra_len, imdb_sampler, Build_Sentences_and_Labels, Build_sample
from Bert_SentimentAnalysis import RunModel_SentimentAnalysis
from uuid import uuid4

parser = argparse.ArgumentParser(description='Imdb_bert')
parser.add_argument('--epochs', type=int, default=30)
parser.add_argument('--batch_size', type=int, default=1)
parser.add_argument('--lr', type=float, default=1e-5)
parser.add_argument('--bert_model', type=str, default='bert-base-cased')
parser.add_argument('--dropout', type=float, default=0.2)
parser.add_argument('--test', type=int, default=0)
parser.add_argument('--debug', type=int, default=0)
args = parser.parse_args()

if not args.debug:
    pdb.set_trace = lambda: None

RUN_ID = str(uuid4())[0:5]
print('RUN_ID : {}'.format(RUN_ID))

PATH_PROJECT = os.getcwd()
PATH_DATA = os.path.join(PATH_PROJECT, 'data')
PATH_MODEL = os.path.join(PATH_PROJECT, 'models', 'imdb')
PATH_REPORT = os.path.join(PATH_PROJECT, 'report', 'imdb')
pdb.set_trace()

if not os.path.exists(PATH_MODEL):
    os.mkdir(PATH_MODEL)

if not os.path.exists(PATH_REPORT):
    os.mkdir(PATH_REPORT)
    os.mkdir(os.path.join(PATH_REPORT, 'runs'))

pdb.set_trace()

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
tokenizer = BertTokenizer.from_pretrained(args.bert_model, do_lower_case=False)

data = pd.read_csv(os.path.join(PATH_DATA, 'IMDB_dataset.csv'), sep=',')

x_train, y_train = data['review'].iloc[:25000], data['sentiment'].iloc[:25000].values
x_test, y_test = data['review'].iloc[25000:].reset_index(drop=True), data['sentiment'].iloc[25000:].values

if args.test:
    print('Train sample :')
    extra_len(x_train, tokenizer)
    print('Test sample :')
    extra_len(x_test, tokenizer)


x_train, y_train, _ = imdb_sampler(x_train, y_train, tokenizer)
x_test, y_test, doc = imdb_sampler(x_test, y_test, tokenizer)
print(doc)


model = BertForSequenceClassification.from_pretrained(args.bert_model, num_labels=2)
if args.dropout != 0:
    model.dropout.p = args.dropout

if __name__ == "__main__":
    pdb.set_trace()
    print('Dataset is : {}'.format('imdb'))
    model, epoch_loss, val_loss, accuracy, tpe = RunModel_SentimentAnalysis(x_train, y_train, x_test, y_test,
                                                                            epochs=args.epochs, batch_size=args.batch_size,
                                                                            lr=args.lr, gpu=1, path_model=PATH_MODEL,
                                                                            weight=0, path_report=PATH_REPORT, num_labels=2,
                                                                            patience=3, model=model, doc=doc)

    PATH_PROJECT = os.getcwd()
    PATH_DATA = os.path.join(PATH_PROJECT, 'data')
    test_path = PATH_DATA + '/test_semeval2017-A.txt'

    test_text, test_labels, doc = Build_Sentences_and_Labels(test_path, n_labels=2)
    test_indexed_tokens, X_test, y_test = Build_sample(test_text, test_labels, random_state=None, shuffle=False,
                                                       n_labels=2, bert_model=args.bert_model)

    model.eval()
    test_set = TensorDataset(X_test, y_test)
    test_loader = torch.utils.data.DataLoader(test_set,
                                              batch_size=4,
                                              num_workers=0)

    test_acc = []
    test_batch_loss = []
    correct_output = [0] * 2
    prediction = [0] * 2

    for data, target in test_loader:

        if torch.cuda.is_available():
            data = data.to(device)
            target = target.to(device)

        output = model.forward(data, attention_mask=(data != 0))
        loss = nn.CrossEntropyLoss()(output, target)
        test_batch_loss.append(loss.item())
        test_acc.append(torch.sum(torch.argmax(output, 1) == target).item() / 4)

        for i in range(len(target)):
            prediction[torch.argmax(output, 1)[i].item()] += 1
            if torch.argmax(output, 1)[i] == target[i]:
                correct_output[target[i]] += 1

    recall = [correct_output[i]/torch.sum(y_test == i).item() for i in doc.keys()]
    precision = [correct_output[i]/prediction[i] for i in doc.keys()]
    F1_score = [2*recall[i]*precision[i]/(recall[i] + precision[i]) for i in doc.keys()]
    test_loss = np.mean(test_batch_loss)
    accuracy['test'].append(np.mean(test_acc))
    AvgRecall = np.mean(recall)
    F1_PN = (F1_score[0] + F1_score[-1])/2

    print('Accuracy : {}'.format(accuracy['test'][-1]))
    print('Recall : {}'.format(recall))
    print('Precision : {}'.format(precision))
    print('F1 score : {}'.format(F1_score))
    print('Avg Recall : {}'.format(AvgRecall))
    print('F1 PN : {}'.format(F1_PN))



